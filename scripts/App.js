import React, {Component} from 'react';
import './styles.css';
import List from './list';


export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      todos: [],
      currentTodo: null
    }
    this.delTodo = this.delTodo.bind(this);
    this.toggleDone = this.toggleDone.bind(this);
  }

  handleLocalStorage() {
    if (localStorage.hasOwnProperty("todos")) {
      let value = localStorage.getItem("todos");
      value = JSON.parse(value);
      this.setState({
        todos: value
      })
    }
  }

  componentDidMount() {
    this.handleLocalStorage();
  }

  componentWillUnmount() {
    this.saveStateToLocalStorage();
  }

  saveStateToLocalStorage() {
    localStorage.setItem("todos", JSON.stringify(this.state.todos));
  }

  todoChanged(e) {
    this.setState({
      currentTodo: {value: e.target.value, idx: null, toggleDone: false}
    })
  }

  addNewTodo() {
    const allTodos = this.state.todos;
    const regex = new RegExp("^[^-\\s][a-zA-Z0-9_\\s-]+$");
    const test = regex.test(this.state.currentTodo ? this.state.currentTodo.value : "");
    if (this.state.currentTodo !== "" && test === true) {
      this.state.currentTodo.idx = Math.random().toString(36).substr(2, 16);
      allTodos.push(this.state.currentTodo);
      this.setState({
        todos: allTodos,
        currentTodo: null,
      });
      this.saveStateToLocalStorage();
    }
    else {
      this.setState({
        currentTodo: null
      });
    }
  }

  delTodo(idx) {
    const removeTodo = this.state.todos;
    removeTodo.splice(idx, 1);
    this.setState({
      todos: removeTodo
    });
    this.saveStateToLocalStorage();
  }

  toggleDone(e) {
    let todoIdx = e.target.dataset.idx;
    let todo = this.state.todos.find(x => x.idx === todoIdx);
    if (todo) {
      todo.toggleDone = e.target.checked
    }
    this.setState({
      todos: this.state.todos,
    })
    this.saveStateToLocalStorage();
  }


  render() {
    return (<div className="main">
      <h1>ToDo</h1>
      <input type="text"
             id="userInput"
             maxLength="35"
             value={this.state.currentTodo ? this.state.currentTodo.value : ""}
             onChange={(e) => this.todoChanged(e)}/>
      <button onClick={() => this.addNewTodo()}>Add</button>
      <List values={this.state.todos}
            removeMethod={this.delTodo}
            toggleDone={this.toggleDone}/>
    </div>)
  }
}
