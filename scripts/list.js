import React from 'react';
import './styles.css';

export default function List({values, removeMethod, toggleDone}) {


  return (<ul>
    {values.map((todo, idx) =>
      <li key={idx}>
        <div data-idx={todo.idx}
             className={todo.toggleDone ? "line-through" : "todo-wrapper"}>
          <input type="checkbox"  checked={todo.toggleDone} data-idx={todo.idx} onChange={(e) => toggleDone(e)}/>
          <p>{todo.value}</p>
        </div>
        <button className="delBtn" onClick={() => {
          removeMethod(idx)
        }}>X
        </button>
      </li>)}
  </ul>)

}
