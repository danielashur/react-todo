const webpack = require('webpack');
const WebpackDevServer = require('webpack-dev-server');
const config = require('./webpack.config');

let ip = '127.0.0.1';
if(process.env.APP_ENV === "production") {
   ip = '0.0.0.0';
}
let port = 80;
new WebpackDevServer(webpack(config), {
  publicPath: config.output.publicPath,
  hot: true,
  disableHostCheck: true,
  historyApiFallback: true
}).listen(port, ip, (err) => {
  if (err) {
    console.log(err);
  }
  console.log(`Listening at ${ip}:${port}`);
});


